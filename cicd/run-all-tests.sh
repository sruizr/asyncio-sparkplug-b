#!/usr/bin/env sh

poetry run pytest --cov=aiospb --cov-report=html --cov-report=term tests
