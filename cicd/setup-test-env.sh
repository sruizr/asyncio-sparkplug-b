#!/usr/bin/env bash
#!/bin/bash

# Parse arguments to set the flags
while [[ "$#" -gt 0 ]]; do
    case $1 in
        --clean-before) docker rm -vf $(docker ps -aq);;
    esac
    shift
done

docker compose -f cicd/docker-compose.yml up -d
