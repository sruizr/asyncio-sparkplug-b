import asyncio
import base64
import os

import pytest

from aiospb.data import DataType, WriteRequest
from aiospb.hosts import HostBridge
from aiospb.mqtt import SpbMessage
from aiospb.mqtt.core import MqttConfig
from aiospb.mqtt.paho import PahoMqttClient
from aiospb.nodes import EdgeNode, MetricCore
from aiospb.nodes.messages import MqttNodeCarrier
from aiospb.nodes.scanning import Scanner
from aiospb.nodes.stores import FsHistoricalStore, OnmemHistoricalStore
from tests.fakes import FakeMetricsNet

_NODE_NAME = "group/node"
_HOSTNAME = "primary-host"
_SCAN_RATE = 100
_MQTT_CONFIG = MqttConfig(
    hostname="0.0.0.0",
    port=1883,
    credentials=base64.b64encode(b"test_user:password").decode(),
)


@pytest.fixture
def mqtt_carrier():
    return MqttNodeCarrier(PahoMqttClient(_MQTT_CONFIG), _NODE_NAME)


@pytest.fixture
def metrics_net():
    return FakeMetricsNet()


@pytest.fixture
def scanner(metrics_net):
    metrics_net.add_core(MetricCore("Node Control/Scan Rate", DataType.Int64))
    metrics_net.set_value(_SCAN_RATE, "Node Control/Scan Rate")
    metrics_net.add_core(MetricCore("Metric/Name", DataType.String, alias=1))
    metrics_net.set_value("value", "Metric/Name")
    return Scanner(metrics_net)


@pytest.fixture
def store():
    return OnmemHistoricalStore()


@pytest.fixture
async def edge_node(mqtt_carrier, scanner, store):
    node = EdgeNode(_NODE_NAME, mqtt_carrier, scanner, store, _HOSTNAME)
    yield node
    await node.terminate_session()


@pytest.fixture
async def host_bridge():
    host = HostBridge(_HOSTNAME, PahoMqttClient(_MQTT_CONFIG))
    yield host
    await host.terminate_session()


class UseCase_EdgeNodeAndHostBridgeCoupled:
    def setup_method(self):
        self.host_messages = []

    async def add_message_to_host(self, message: SpbMessage):
        self.host_messages.append(message)

    async def scenario_node_comunication_to_host(
        self, edge_node: EdgeNode, host_bridge: HostBridge, metrics_net
    ):
        host_bridge.observe_nodes(self.add_message_to_host)

        await host_bridge.establish_session(["group"])
        await edge_node.establish_session()

        await asyncio.sleep(0.2)

        nbirth = self.host_messages[0]
        assert nbirth.is_a("NBIRTH")
        payload = nbirth.payload.to_dict()
        assert (
            len(payload["metrics"]) == 5
        )  # bseq, reboot, scan_rate, rebirth, Metric/Name

        # Metric updated to value "new-value"
        metrics_net.set_value("new-value", "Metric/Name")
        await asyncio.sleep(0.2)
        ndata = self.host_messages[1]
        assert ndata.is_a("NDATA")
        payload = ndata.payload.to_dict()
        assert payload["metrics"][0]["value"] == "new-value"

        # Metric is Staled
        metrics_net.set_value(None, "Metric/Name")
        await asyncio.sleep(0.2)
        ndata = self.host_messages[2]
        assert ndata.is_a("NDATA")
        payload = ndata.payload.to_dict()
        assert payload["metrics"][0]["properties"]["quality"]["value"] == 500
        assert payload["seq"] == 2

        await edge_node.terminate_session()
        await asyncio.sleep(0.4)

        ndeath = self.host_messages[3]
        assert ndeath.is_a("NDEATH")
        payload = ndeath.payload.to_dict()
        assert payload["metrics"][0]["value"] == 0
        assert payload["metrics"][0]["name"] == "bdSeq"

    async def scenario_node_goes_connected_when_host_is_offline(
        self, edge_node, host_bridge
    ):
        host_bridge.observe_nodes(self.add_message_to_host)

        await host_bridge.establish_session(["group"])
        await edge_node.establish_session()

        await asyncio.sleep(0.2)

        await host_bridge.terminate_session()
        await asyncio.sleep(0.2)
        assert edge_node.state == "connected"

        await host_bridge.establish_session(["group"])
        await asyncio.sleep(0.5)
        assert edge_node.state == "reporting"

    async def scenario_node_process_commands(
        self, edge_node: EdgeNode, host_bridge: HostBridge, metrics_net
    ):
        host_bridge.observe_nodes(self.add_message_to_host)

        await host_bridge.establish_session(["group"])
        await edge_node.establish_session()
        await asyncio.sleep(0.5)

        await host_bridge.write_metrics(
            _NODE_NAME,
            [
                WriteRequest("updated-value", "Metric/Name"),
            ],
        )

        await asyncio.sleep(0.1)
        confirmation = self.host_messages[-1]
        assert confirmation.is_a("NDATA")
        metrics = confirmation.payload.to_dict()["metrics"]
        assert metrics[0]["value"] == "updated-value"
        assert metrics_net.writtings == [("updated-value", "", 1)]

    async def scenario_storing_historical_in_disk(
        self, tmp_path, host_bridge, mqtt_carrier, scanner, metrics_net
    ):
        filename = tmp_path / "store.csv"
        edge_node = EdgeNode(
            "group/node",
            mqtt_carrier,
            scanner,
            FsHistoricalStore(filename=filename),
            primary_hostname=_HOSTNAME,
        )

        host_bridge.observe_nodes(self.add_message_to_host)
        await host_bridge.establish_session(["group"])
        await edge_node.establish_session()
        await asyncio.sleep(0.3)  # scan all stuff

        await host_bridge.terminate_session()
        await asyncio.sleep(0.2)  # Save into file
        metrics_net.set_value("stored-value", "Metric/Name")
        await asyncio.sleep(0.2)  # Save into file

        assert edge_node.state == "connected"
        assert os.listdir(tmp_path) == ["store.csv"]

        await host_bridge.establish_session()
        metrics_net.set_value("rebirth-value", "Metric/Name")

        await asyncio.sleep(0.2)  # next sample with content
        metrics_net.set_value("changed-value", "Metric/Name")
        await asyncio.sleep(0.2)  # next sample with content
        assert edge_node.state == "reporting"
        ndata = None
        for message in self.host_messages:
            if message.payload.seq == 2:
                ndata = message
        assert ndata is not None
        payload = ndata.payload.to_dict()
        assert payload["metrics"][-2]["is_historical"]
        assert not os.listdir(tmp_path)

        await edge_node.terminate_session()
