from aiospb.data import DataType, Metric, PropertySet
from aiospb.mqtt.core import NodePayload, SpbMessage, Topic
from aiospb.nodes import MetricCore


class MetricCoreBuilder:
    def __init__(self):
        self._name = "Metric/Name"
        self._properties = {}
        self._is_transient = False
        self._alias = 0
        self._data_type = DataType.Int16

    def with_name(self, value):
        self._name = value
        return self

    def with_properties(self, **properties):
        self._properties = properties
        return self

    def with_alias(self, value):
        self._alias = value
        return self

    def is_transient(self):
        self._is_transient = True
        return self

    def with_scan_rate(self, value):
        self._properties["scan_rate"] = {"value": value, "dataType": "Int64"}
        return self

    def scan_rate(self):
        self._name = "Node Control/Scan Rate"
        self._data_type = DataType.Int64
        self._properties = {}
        self._alias = 0
        self._is_transient = False
        return self

    def reboot(self):
        self._name = "Node Control/Reboot"
        self._data_type = DataType.Boolean
        self._properties = {}
        self._alias = 0
        self._is_transient = False
        return self

    def rebirth(self):
        self._name = "Node Control/Rebirth"
        self._data_type = DataType.Boolean
        self._properties = {}
        self._alias = 0
        self._is_transient = False
        return self

    def db_seq(self):
        self._name = "dbSeq"
        self._data_type = DataType.Int64
        self._properties = {}
        self._alias = 0
        self._is_transient = False
        return self

    def build(self):
        props = PropertySet.from_dict(self._properties)
        return MetricCore(
            self._name,
            self._data_type,
            props,
            self._alias,
            self._is_transient,
        )


class MetricBuilder:
    def __init__(self):
        self._ts = 1000
        self._value = 1
        self._data_type = DataType.Int16
        self._alias = 0
        self._name = "Metric/Name"
        self._properties = PropertySet()
        self._is_transient = False
        self._is_historical = False

    def with_metric(self, value):
        self._data_type = value.data_type
        self._alias = value.alias
        self._properties = value.properties
        self._name = value.name
        self._alias = value.alias
        self._is_transient = value.is_transient

        return self

    def with_ts(self, value):
        self._ts = value
        return self

    def with_name(self, value):
        self._name = value
        return self

    def with_value(self, value):
        self._value = value
        return self

    def with_datatype(self, value):
        self._data_type = DataType[value]
        return self

    def with_alias(self, value):
        self._alias = value
        return self

    def build(self):
        return Metric(
            self._ts,
            self._value,
            self._data_type,
            self._alias,
            self._name,
            self._properties,
            self._is_transient,
            self._is_historical,
        )


class NodeMessageBuilder:
    def __init__(self):
        self._ts = 1000
        self._metrics = []
        self._seq = 1
        self._type = "NDATA"
        self._node_name = "group/node"

    def from_node(self, name):
        self._node_name = name
        return self

    def of_type(self, value):
        self._type = value
        return self

    def with_seq(self, value):
        self._seq = value
        return self

    def with_metric(self, value):
        self._metrics.append(value)
        return self

    def with_bd_seq(self, value):
        self._metrics.append(Metric(self._ts, value, DataType.Int32, name="bdSeq"))
        return self

    def with_ts(self, value):
        self._ts = value
        return self

    def build(self):
        return SpbMessage(
            Topic.from_component(self._node_name, self._type),
            NodePayload(self._ts, self._metrics, self._seq),
        )


class Given:
    @property
    def a_metric_core(self):
        return MetricCoreBuilder()

    @property
    def a_metric(self):
        return MetricBuilder()

    @property
    def a_node_message(self):
        return NodeMessageBuilder()
