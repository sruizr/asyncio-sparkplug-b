import base64
import os

from aiospb.mqtt.core import MqttConfig

CERTIFICATE_CONTENT = """-----BEGIN CERTIFICATE-----
certificate-content-with-more-than-64-characters-to-be-splitted-
in-the-code
-----END CERTIFICATE-----"""


class A_MqttConfig:
    def given_a_mqtt_config(self):
        credentials = base64.b64encode(b"username:password").decode()
        ca_cert = "certificate-content-with-more-than-64-characters-to-be-splitted-in-the-code"
        return MqttConfig("hostname", 1111, credentials, ca_cert, 40)

    def should_generate_crt_file(self):
        config = self.given_a_mqtt_config()

        filename = config.deploy_certificate_file()
        with open(filename, "r") as f:
            content = f.read()
            assert content == CERTIFICATE_CONTENT

        os.unlink(filename)

    def should_decode_login_information(self):
        config = self.given_a_mqtt_config()

        username, password = config.login_info()

        assert username == "username"
        assert password == "password"
