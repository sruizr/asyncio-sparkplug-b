import asyncio
import base64
from unittest.mock import MagicMock

import pytest
from aiomqtt import Client

from aiospb.data import DataType
from aiospb.mqtt import HostPayload, Metric, MqttError, SpbMessage, Topic, Will
from aiospb.mqtt.core import MqttConfig
from aiospb.mqtt.paho import PahoMqttClient
from aiospb.nodes.messages import NodePayload

# Run cicd/setup-env.sh to create a local mosquitto broker and run the test

_CONFIG = MqttConfig(
    "0.0.0.0", 1883, base64.b64encode(b"test_user:password").decode(), keepalive=2
)


@pytest.fixture
def client(monkeypatch):
    aiomqtt_module = MagicMock()
    monkeypatch.setattr("aiospb.mqtt.paho.aiomqtt", aiomqtt_module)
    client = aiomqtt_module.Client.return_value = MagicMock(spec=Client)

    return client


class A_PahoMqttClient:
    @pytest.mark.integration
    async def should_publish_state_messages(self):
        app_client = PahoMqttClient(_CONFIG)
        node_client = PahoMqttClient(_CONFIG)
        assert node_client.keepalive == 2
        bd_seq = Metric(1000, 0, DataType.Int64, name="bdSeq")

        await node_client.connect(
            "group/node",
            will=Will(
                SpbMessage(
                    Topic.from_component("group/node", "NDEATH"),
                    NodePayload(1000, [bd_seq]),
                ),
                1,
                False,
            ),
        )

        await app_client.connect(
            "sgclima",
            will=Will(
                SpbMessage(
                    Topic.from_component("sgclima", "STATE"), HostPayload(1000, False)
                ),
                1,
                True,
            ),
        )
        await app_client.subscribe("spBv1.0/group/+/node", 0)
        await node_client.subscribe("spBv1.0/STATE/sgclima", 1)

        state_message = SpbMessage(
            Topic.from_component("sgclima", "STATE"), HostPayload(1010, True)
        )
        await app_client.publish(state_message, 1, True)

        birth_message = SpbMessage(
            Topic.from_component("group/node", "NBIRTH"), NodePayload(1010, [bd_seq], 1)
        )
        await node_client.publish(birth_message, 0, False)

        app_message = await app_client.deliver_message()
        assert app_message == birth_message

        # await node_client.deliver_message()  # Clean death certificate of host
        node_message = await node_client.deliver_message()
        assert node_message == state_message

        await node_client.disconnect()
        await app_client.disconnect()

    async def connect_node_client(self, node_client):
        await node_client.connect(
            "component",
            will=Will(
                SpbMessage(
                    Topic.from_component("group/node", "NDEATH"),
                    NodePayload(1000, [Metric(1000, 0, DataType.Int64, name="bdSeq")]),
                ),
                1,
                False,
            ),
        )

    async def should_raise_mqtt_error_if_exception_when_connect(self, client):
        node_client = PahoMqttClient(_CONFIG)
        client.__aenter__.side_effect = Exception()

        with pytest.raises(MqttError):
            await self.connect_node_client(node_client)

        assert not node_client.is_connected

    async def should_raise_mqtt_error_if_exception_when_delivery_changes(self, client):
        node_client = PahoMqttClient(_CONFIG)
        client.messages.__anext__.side_effect = Exception()
        await self.connect_node_client(node_client)

        with pytest.raises(MqttError):
            await node_client.deliver_message()

        assert not node_client.is_connected

    async def should_raise_mqtt_error_if_exception_when_publish(self, client):
        node_client = PahoMqttClient(_CONFIG)
        client.publish.side_effect = Exception()

        await self.connect_node_client(node_client)

        with pytest.raises(MqttError):
            await node_client.publish(
                SpbMessage(
                    Topic.from_component("sgclima", "STATE"), HostPayload(1010, True)
                ),
                1,
                True,
            )

        assert not node_client.is_connected

    async def should_raise_mqtt_error_if_exception_when_subscribe(self, client):
        node_client = PahoMqttClient(_CONFIG)
        client.subscribe.side_effect = Exception()

        await self.connect_node_client(node_client)

        with pytest.raises(MqttError):
            await node_client.subscribe("spBv1.0/STATE/+", qos=1)

        assert not node_client.is_connected

    async def should_raise_runtime_if_not_connected_before(self, client):
        node_client = PahoMqttClient(_CONFIG)

        with pytest.raises(RuntimeError):
            await node_client.publish(
                SpbMessage(
                    Topic.from_component("sgclima", "STATE"), HostPayload(1010, True)
                ),
                1,
                True,
            )

        with pytest.raises(RuntimeError):
            await node_client.deliver_message()

        with pytest.raises(RuntimeError):
            await node_client.subscribe("spBv1.0/STATE/+", qos=1)

        await node_client.disconnect()

    async def should_try_3_times_to_publish_if_timeout(self, client):
        client.publish.side_effect = asyncio.TimeoutError
        node_client = PahoMqttClient(_CONFIG)
        await self.connect_node_client(node_client)

        with pytest.raises(MqttError):
            await node_client.publish(
                SpbMessage(
                    Topic.from_component("sgclima", "STATE"), HostPayload(1010, True)
                ),
                1,
                True,
            )
