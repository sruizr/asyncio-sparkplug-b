import asyncio
from unittest.mock import MagicMock

import pytest

from aiospb import Clock
from aiospb.data import DataType, Metric, PropertySet, PropertyValue, WriteRequest
from aiospb.hosts.commands import (
    CommandRequest,
    CommandResponse,
    NodeCommander,
    WritingResolution,
)
from aiospb.mqtt.core import MqttClient, MqttError, NodePayload, SpbMessage, Topic
from tests.fakes import FakeClock

_NODE_NAME = "group/node"
_TS = 1000


@pytest.fixture
def clock():
    return FakeClock(_TS)


@pytest.fixture
def mqtt_client():
    return MagicMock(spec=MqttClient)


@pytest.fixture
def commander(mqtt_client, clock):
    commander_ = NodeCommander(_NODE_NAME, mqtt_client, clock)
    commander_.handle_node_message(
        SpbMessage(
            Topic.from_component(_NODE_NAME, "NBIRTH"),
            NodePayload(
                _TS,
                [
                    Metric(_TS, 1, DataType.Int16, name="Without/Alias"),
                    Metric(_TS, 2, DataType.Int16, alias=1, name="With/Alias"),
                ],
                seq=0,
            ),
        )
    )

    return commander_


class A_NodeCommander:
    def given_a_command_request(self, name="Without/Alias", alias=0):
        return CommandRequest([WriteRequest(1, name, alias=alias)])

    async def should_return_edge_node_is_offline_resolution(self, mqtt_client, clock):
        commander = NodeCommander(_NODE_NAME, mqtt_client, clock)
        response = await commander.execute(self.given_a_command_request())

        assert response == CommandResponse(_TS, [WritingResolution.EdgeIsOffline])

    async def should_return_host_is_disconected_resolution(
        self, commander, mqtt_client
    ):
        mqtt_client.publish.side_effect = MqttError()

        response = await commander.execute(self.given_a_command_request())

        assert response == CommandResponse(_TS, [WritingResolution.HostIsOffline])

    def when_handle_change_confirmation(
        self, commander, alias, name, value, quality=None
    ):
        properties = (
            PropertySet.from_kwargs(quality=PropertyValue(quality, DataType.Int32))
            if quality is not None
            else {}
        )
        commander.handle_node_message(
            SpbMessage(
                Topic.from_component(_NODE_NAME, "NDATA"),
                NodePayload(
                    _TS,
                    [
                        Metric(
                            _TS,
                            value,
                            DataType.Int16,
                            alias=alias,
                            name=name,
                            properties=properties,
                        )
                    ],
                ),
            )
        )

    async def should_resolve_lack_of_quality(self, commander):
        command = asyncio.create_task(
            commander.execute(
                CommandRequest([WriteRequest(2, metric_name="Without/Alias")], 1)
            )
        )
        await asyncio.sleep(0)

        self.when_handle_change_confirmation(
            commander, alias=0, name="Without/Alias", value=2
        )

        response = await command
        assert response == CommandResponse(_TS, [WritingResolution.GoodWriting])

    async def should_resolve_good_writing(self, commander):
        command = asyncio.create_task(
            commander.execute(
                CommandRequest([WriteRequest(2, metric_name="Without/Alias")], 1)
            )
        )
        await asyncio.sleep(0)

        self.when_handle_change_confirmation(
            commander, alias=0, name="Without/Alias", value=2, quality=192
        )

        response = await command
        assert response == CommandResponse(_TS, [WritingResolution.GoodWriting])

    async def should_resolve_stale_writing(self, commander):
        command = asyncio.create_task(
            commander.execute(CommandRequest([WriteRequest(2, alias=1)], 1))
        )
        await asyncio.sleep(0)

        self.when_handle_change_confirmation(
            commander, alias=1, name="", value=2, quality=500
        )

        response = await command
        assert response == CommandResponse(_TS, [WritingResolution.StaleWriting])

    async def should_resolve_bad_writing(self, commander):
        command = asyncio.create_task(
            commander.execute(CommandRequest([WriteRequest(2, alias=1)], 1))
        )
        await asyncio.sleep(0)

        self.when_handle_change_confirmation(
            commander, alias=1, name="", value=2, quality=0
        )

        response = await command
        assert response == CommandResponse(_TS, [WritingResolution.BadWriting])

    async def should_resolve_no_metric_found(self, commander):
        response = await commander.execute(
            CommandRequest([WriteRequest(2, alias=2)], 1)
        )
        assert response == CommandResponse(_TS, [WritingResolution.MetricNotFound])

        response = await commander.execute(
            CommandRequest([WriteRequest(2, metric_name="Dont/Exist")], 1)
        )
        assert response == CommandResponse(_TS, [WritingResolution.MetricNotFound])

    async def should_resolve_writing_timeout(self, commander):
        response = await commander.execute(
            CommandRequest([WriteRequest(2, alias=1)], 0)
        )
        assert response == CommandResponse(_TS, [WritingResolution.WritingTimeout])

    async def should_not_write_property(self, commander):
        commander.handle_node_message(
            SpbMessage(
                Topic.from_component(_NODE_NAME, "NBIRTH"),
                NodePayload(
                    _TS,
                    [
                        Metric(
                            _TS, True, DataType.Boolean, name="Properties/Is Connected"
                        )
                    ],
                    1,
                ),
            )
        )

        response = await commander.execute(
            CommandRequest([WriteRequest(False, metric_name="Properties/Is Connected")])
        )

        assert response == CommandResponse(_TS, [WritingResolution.MetricNotFound])
