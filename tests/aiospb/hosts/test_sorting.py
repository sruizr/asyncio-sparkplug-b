from unittest.mock import MagicMock

import pytest

import aiospb.hosts.sorting as s
from aiospb.hosts.sorting import NodeMessageSorter
from tests.builders import Given
from tests.fakes import FakeClock

_NODE_NAME = "group/node"
_TS = 1000
_REORDER_TIME = 0.1
given = Given()


@pytest.fixture
def logger():
    logger_ = s.logger
    s.logger = MagicMock()
    yield s.logger
    s.logger = logger_


@pytest.fixture
def clock():
    return FakeClock(_TS)


@pytest.fixture
def sorter(clock):
    return NodeMessageSorter(_NODE_NAME, clock, _REORDER_TIME)


class A_NodeMessageSorter:
    def given_a_node_message(self):
        return given.a_node_message.from_node(_NODE_NAME)

    def given_a_sequence_of_messages(self, bd_seq=0, seq=10):
        seq_ = [seq + i for i in range(4)]
        birth = (
            self.given_a_node_message()
            .of_type("NBIRTH")
            .with_bd_seq(bd_seq)
            .with_seq(seq_[0])
            .build()
        )
        data_1 = self.given_a_node_message().of_type("NDATA").with_seq(seq_[1]).build()
        data_2 = self.given_a_node_message().of_type("NDATA").with_seq(seq_[2]).build()
        data_3 = self.given_a_node_message().of_type("NDATA").with_seq(seq_[3]).build()
        death = (
            self.given_a_node_message().of_type("NDEATH").with_bd_seq(bd_seq).build()
        )
        return [birth, data_1, data_2, data_3, death]

    def should_reorder_messages(self, sorter):
        msgs = self.given_a_sequence_of_messages()
        ordered = []
        for message in (msgs[0], msgs[2], msgs[1], msgs[3], msgs[4]):
            sorter.register_message(message)
            ordered.extend(sorter.nexts())

        assert ordered == msgs

    async def should_loose_data_if_death_is_recieved_before(self, sorter):
        msgs = self.given_a_sequence_of_messages()
        output = []
        for message in (msgs[0], msgs[4], msgs[1]):
            sorter.register_message(message)
            output.extend(sorter.nexts())

        assert output == [msgs[0], msgs[4]]

    async def should_raise_timeout_error_if_desorder_is_for_long_time(
        self, sorter, clock
    ):
        msgs = self.given_a_sequence_of_messages()

        sorter.register_message(msgs[0])
        sorter.nexts()

        sorter.register_message(msgs[3])
        assert not sorter.nexts()

        sorter.register_message(msgs[2])
        clock.sleep(5)
        with pytest.raises(TimeoutError):
            sorter.nexts()

    async def should_loose_data_without_birth(self, sorter, logger):
        msgs = self.given_a_sequence_of_messages()

        sorter.register_message(msgs[1])  # NDATA
        sorter.register_message(msgs[0])  # NBIRTH

        logger.warning.assert_called()

    async def should_remove_unsorted_messages_after_rebirth(self, sorter, logger):
        for msg in self.given_a_sequence_of_messages(seq=1):
            sorter.register_message(msg)

        msgs = self.given_a_sequence_of_messages(seq=10)
        sorter.register_message(msgs[0])

        assert sorter.nexts() == [msgs[0]]
        logger.warning.assert_called()
