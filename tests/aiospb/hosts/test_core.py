import asyncio
from unittest.mock import AsyncMock, MagicMock

import pytest

from aiospb import Clock
from aiospb.hosts import HostBridge
from aiospb.hosts.commands import NodeCommander
from aiospb.hosts.sorting import NodeMessageSorter
from aiospb.mqtt import HostPayload, MqttClient, MqttError, SpbMessage, Topic, Will
from tests.builders import Given

given = Given()

_TS = 1000
_HOSTNAME = "primary-host"
_GROUPS = "group"
_NODE_NAME = "group/node"
_REORDER_TIMEOUT = 3.1
given = Given()


@pytest.fixture
def sorter(monkeypatch):
    sorter_ = MagicMock(spec=NodeMessageSorter)
    mock = MagicMock(return_value=sorter_)
    monkeypatch.setattr("aiospb.hosts.core.NodeMessageSorter", mock)
    return sorter_


@pytest.fixture
def commander(monkeypatch):
    commander_ = MagicMock(spec=NodeCommander)
    mock = MagicMock(return_value=commander)

    monkeypatch.setattr("aiospb.hosts.core.NodeCommander", mock)
    return commander_


@pytest.fixture
def clock():
    clock = MagicMock(spec=Clock)
    clock.timestamp.return_value = _TS
    return clock


@pytest.fixture
def mqtt_client():
    mock = MagicMock(spec=MqttClient)
    mock.messages = asyncio.Queue()

    async def deliver_message():
        return await mock.messages.get()

    mock.deliver_message = deliver_message
    return mock


@pytest.fixture
async def bridge(mqtt_client, clock):
    app = HostBridge(
        _HOSTNAME,
        mqtt_client,
        reorder_timeout=_REORDER_TIMEOUT,
        groups=_GROUPS,
        clock=clock,
    )
    assert app.hostname == _HOSTNAME
    await app.establish_session(["group"])
    yield app
    await app.terminate_session()


class A_HostBridge:
    @pytest.mark.usefixtures("bridge")
    async def should_connect_when_establish_session(self, mqtt_client):
        mqtt_client.connect.assert_called_with(
            _HOSTNAME,
            will=Will(
                SpbMessage(
                    Topic.from_component(_HOSTNAME, "STATE"),
                    HostPayload(_TS, False),
                ),
                qos=1,
                retain=True,
            ),
        )

    @pytest.mark.usefixtures("bridge")
    async def should_subscribe_to_node_topics(self, mqtt_client):
        mqtt_client.subscribe.assert_called_with("spBv1.0/group/+/+", qos=1)

    @pytest.mark.usefixtures("bridge")
    async def should_birth_with_state_true_when_establish_session(self, mqtt_client):
        mqtt_client.publish.assert_called_with(
            SpbMessage(Topic(f"spBv1.0/STATE/{_HOSTNAME}"), HostPayload(_TS, True)),
            qos=1,
            retain=True,
        )

    async def should_send_death_when_terminate_session(self, bridge, mqtt_client):
        await bridge.terminate_session()

        mqtt_client.publish.assert_called_with(
            SpbMessage(Topic(f"spBv1.0/STATE/{_HOSTNAME}"), HostPayload(_TS, False)),
            qos=1,
            retain=True,
        )

    async def should_disconnect_when_terminate_session(self, bridge, mqtt_client):
        await bridge.terminate_session()

        mqtt_client.disconnect.assert_called_with()

    async def should_disconnect_even_with_mqtt_error(self, bridge, mqtt_client):
        mqtt_client.disconnect.side_effect = MqttError()

        await bridge.terminate_session()

    def given_a_birth_certificate(self):
        return (
            given.a_node_message.from_node(_NODE_NAME)
            .of_type("NBIRTH")
            .with_bd_seq(0)
            .with_seq(1)
            .build()
        )

    async def should_notify_birth_messages(self, bridge, mqtt_client):
        birth_certificate = self.given_a_birth_certificate()
        await mqtt_client.messages.put(birth_certificate)

        messages = []

        async def callback(message):
            messages.append(message)

        # two callbacks
        bridge.observe_nodes(callback)
        bridge.observe_nodes(callback, _NODE_NAME)
        await asyncio.sleep(0.01)

        assert messages[0] == birth_certificate
        assert messages[1] == birth_certificate
        assert len(messages) == 2

    async def should_not_stop_if_exception_by_observer(self, bridge, mqtt_client):
        callback_with_error = AsyncMock()
        callback_with_error.side_effect = Exception("Something went wrong")
        bridge.observe_nodes(callback_with_error)

        callback = AsyncMock()
        bridge.observe_nodes(callback)

        birth_certificate = self.given_a_birth_certificate()
        await mqtt_client.messages.put(birth_certificate)
        await asyncio.sleep(0.01)

        callback.assert_called_with(birth_certificate)

    async def should_rebirth_if_sorter_has_timeout(self, sorter, mqtt_client, clock):
        bridge = HostBridge(_HOSTNAME, mqtt_client, clock=clock)
        callback = AsyncMock()
        bridge.observe_nodes(callback)
        await bridge.establish_session()

        sorter.nexts.side_effect = TimeoutError()
        birth_certificate = self.given_a_birth_certificate()
        await mqtt_client.messages.put(birth_certificate)
        await asyncio.sleep(0)

        mqtt_client.publish.assert_called_with(
            given.a_node_message.from_node(_NODE_NAME)
            .with_ts(_TS)
            .of_type("NCMD")
            .with_seq(None)
            .with_ts(_TS)
            .with_metric(
                given.a_metric.with_ts(_TS)
                .with_name("Node Control/Rebirth")
                .with_value(True)
                .with_datatype("Boolean")
                .build()
            )
            .build(),
            qos=0,
            retain=False,
        )

        await bridge.terminate_session()

    async def should_not_establish_session_if_mqtt_error(self, mqtt_client):
        bridge = HostBridge(_HOSTNAME, mqtt_client)
        mqtt_client.connect.side_effect = MqttError()

        with pytest.raises(MqttError):
            await bridge.establish_session()

        assert not bridge.is_online

    async def should_terminate_even_if_mqtt_error(self, mqtt_client):
        bridge = HostBridge(_HOSTNAME, mqtt_client)
        await bridge.establish_session()
        assert bridge.is_online

        mqtt_client.publish.side_effect = MqttError()
        await bridge.terminate_session()

        assert not bridge.is_online

    async def should_process_mqtt_error_when_recieving(self, mqtt_client):
        mqtt_client.deliver_message = AsyncMock()
        mqtt_client.deliver_message.side_effect = MqttError()

        bridge = HostBridge(_HOSTNAME, mqtt_client)

        await bridge.establish_session()

        await asyncio.sleep(0)
        assert not bridge.is_online

    async def should_process_any_error_when_recieving(self, mqtt_client):
        mqtt_client.deliver_message = AsyncMock()
        mqtt_client.deliver_message.side_effect = Exception()

        bridge = HostBridge(_HOSTNAME, mqtt_client)

        await bridge.establish_session()

        await asyncio.sleep(0)
        assert not bridge.is_online
