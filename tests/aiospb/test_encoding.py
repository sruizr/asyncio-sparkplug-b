from aiospb.data import DataType
from aiospb.mqtt import Metric, NodePayload
from aiospb.mqtt.encoding.json import JsonEncoder


class A_JsonEncoder:
    def should_encode_dict_to_json(self):
        payload = NodePayload(
            1000, [Metric(900, True, DataType.Boolean, 1, "Metric/Name")], 1
        )
        assert (
            JsonEncoder().encode(payload)
            == b'{"metrics": [{"alias": 1, "dataType": "Boolean", "name": "Metric/Name", "timestamp": 900, "value": true}], "seq": 1, "timestamp": 1000}'
        )

    def should_decode_json_to_dict(self):
        payload = b'{"metrics": [{"alias": 1, "dataType": "Boolean", "name": "Metric/Name", "timestamp": 900, "value": true}], "seq": 1, "timestamp": 1000}'
        assert JsonEncoder().decode(payload) == NodePayload(
            1000, [Metric(900, True, DataType.Boolean, 1, "Metric/Name")], 1
        )
