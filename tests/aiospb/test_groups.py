from unittest.mock import AsyncMock, MagicMock, call, patch

from aiospb import UtcClock
from aiospb.groups import EdgeNode, MqttServer, NodeFactory, NodesGroup


def test_real_clock():
    clock = UtcClock()
    assert type(clock.now()) is int
    clock.sleep(0.0001)


class A_NodeGroup:
    def setup_method(self):
        self.node_factory = MagicMock(spec=NodeFactory)

    def should_list_node_names_after_setup(self):
        self.node_factory.node_names = ["node1", "node2", "node3"]
        group = NodesGroup("group-name", self.node_factory)

        group.setup(".*1$")

        assert len(group.nodes) == 1
        assert group.nodes[0] == self.node_factory.create.return_value
        self.node_factory.create.assert_called_with("node1")

    def should_start_all_nodes_when_run(self):
        self.node_factory.node_names = ["node1", "node2", "node3"]
        self.node_factory.create.side_effect = nodes = [
            MagicMock(spec=EdgeNode) for _ in range(3)
        ]
        nodes[0].state = "crashed"

        group = NodesGroup("group-name", self.node_factory, 0, 0)
        group.setup()

        group.run(1)

        for node in group.nodes:
            node.establish_session.assert_called_with()
        nodes[0].terminate_session.assert_called_with()
