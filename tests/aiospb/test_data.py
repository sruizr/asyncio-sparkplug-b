from aiospb.data import DataType, PropertySet, PropertyValue

TS = 123


class A_PropertySet:
    def should_construct_from_dict(self):
        assert PropertySet.from_dict(
            {"unit": {"value": "V", "dataType": "String"}}
        ) == PropertySet(("unit",), (PropertyValue("V", DataType.String),))

    def should_construct_from_kwargs(self):
        assert PropertySet.from_kwargs(
            unit=PropertyValue("V", DataType.String)
        ) == PropertySet(("unit",), (PropertyValue("V", DataType.String),))

    def should_parse_to_dict(self):
        assert PropertySet(
            ("unit",), (PropertyValue("V", DataType.String),)
        ).as_dict() == {"unit": {"value": "V", "dataType": "String"}}
