import asyncio
from unittest.mock import AsyncMock

import pytest

from aiospb.data import DataType, Metric, PropertySet, PropertyValue, Quality
from aiospb.nodes.scanning import MetricCore, Scanner
from tests.builders import Given
from tests.fakes import FakeClock, FakeMetricsNet

given = Given()

_TS = 1000
_SCAN_RATE = 10


@pytest.fixture
def clock():
    return FakeClock()


@pytest.fixture
def metrics_net():
    net = FakeMetricsNet()
    net.add_core(MetricCore("Node Control/Scan Rate", DataType.Int64))
    net.set_value(int(_SCAN_RATE), "Node Control/Scan Rate")
    return net


@pytest.fixture
async def scanner(clock, metrics_net):
    scanner_ = Scanner(metrics_net, clock)

    yield scanner_
    await scanner_.stop()


class A_Scanner:
    def when_metrics_net_has_a_metric(self, net, alias=0, metric_name="Metric/Name"):
        core = given.a_metric_core.with_alias(alias).with_name(metric_name).build()
        net.add_core(core)
        net.set_value(1, metric_name=metric_name)
        return core.create_metric(1, _TS, Quality.GOOD)

    async def should_have_scan_rate_metric_to_start(self):
        metrics_net = FakeMetricsNet()
        scanner = Scanner(metrics_net)

        with pytest.raises(ValueError):
            await scanner.start()

    async def should_have_metrics_cores_with_alias_or_name(self, scanner, metrics_net):
        core = given.a_metric_core.with_name("").with_alias(0).build()
        metrics_net.add_core(core)

        with pytest.raises(ValueError):
            await scanner.start()

    async def should_construct_births_when_start_scanning(self, scanner, metrics_net):
        metric = self.when_metrics_net_has_a_metric(metrics_net, alias=1)

        births = await scanner.start()

        assert len(births) == 4
        assert births[0] == Metric(
            _TS, False, DataType.Boolean, name="Node Control/Reboot"
        )
        assert births[1] == Metric(
            _TS, False, DataType.Boolean, name="Node Control/Rebirth"
        )
        assert births[2] == Metric(
            _TS, _SCAN_RATE, DataType.Int64, name="Node Control/Scan Rate"
        )
        assert births[3] == metric

    async def _after_passing_a_scan_rate_period(self, clock, scan_rate=_SCAN_RATE):
        await asyncio.sleep(0)
        clock.sleep(scan_rate / 1000)  # Pass time to scan

    async def should_scan_metrics(self, scanner: Scanner, metrics_net, clock):
        metric = self.when_metrics_net_has_a_metric(metrics_net, alias=1)

        await scanner.start()

        metrics_net.set_value(2, metric.name)  # Change from 1 to 2
        await self._after_passing_a_scan_rate_period(clock)
        changes = await scanner.deliver_changes(1)
        assert changes == [
            Metric(
                _TS + 10,
                2,
                metric.data_type,
                alias=1,
            )
        ]

        metrics_net.set_value(Exception(), metric.name)
        await self._after_passing_a_scan_rate_period(clock)
        changes = await scanner.deliver_changes(0.5)
        assert changes == [
            Metric(
                _TS + _SCAN_RATE * 2,
                2,
                metric.data_type,
                alias=1,
                properties=PropertySet(
                    ("quality",), (PropertyValue(0, DataType.Int32),)
                ),
            )
        ]

        metrics_net.set_value(None, metric.name)
        await self._after_passing_a_scan_rate_period(clock)
        changes = await scanner.deliver_changes(0.5)
        assert changes == [
            Metric(
                _TS + _SCAN_RATE * 3,
                2,
                metric.data_type,
                alias=1,
                properties=PropertySet(
                    ("quality",), (PropertyValue(500, DataType.Int32),)
                ),
            )
        ]

    async def should_scan_no_report_if_no_variations_of_metric(
        self, scanner, metrics_net, clock
    ):
        self.when_metrics_net_has_a_metric(metrics_net, alias=1)
        await scanner.start()

        metrics_net.set_value(None, alias=1)
        await self._after_passing_a_scan_rate_period(clock)
        await scanner.deliver_changes(timeout=0.02)  # Capturing stale

        with pytest.raises(TimeoutError):
            await self._after_passing_a_scan_rate_period(clock)
            await scanner.deliver_changes(timeout=0.05)

    async def should_modify_main_scan_rate_during_scanning(
        self, scanner, metrics_net, clock
    ):
        core = given.a_metric_core.build()
        metrics_net.add_core(core)
        await scanner.start()

        metrics_net.set_value(2, core.name)
        await self._after_passing_a_scan_rate_period(clock)
        changes = await scanner.deliver_changes()
        assert changes[0].timestamp == _TS + _SCAN_RATE

        scanner.scan_rate = 100
        metrics_net.set_value(3, core.name)
        with pytest.raises(TimeoutError):
            await self._after_passing_a_scan_rate_period(clock, _SCAN_RATE)
            await scanner.deliver_changes(0.01)

        await self._after_passing_a_scan_rate_period(clock, 100 - _SCAN_RATE)
        changes = await scanner.deliver_changes()
        assert changes[0].timestamp == _TS + _SCAN_RATE + 100

    async def should_dont_modify_main_scan_rate_with_diferent_scan_rate(
        self, scanner, metrics_net, clock
    ):
        core = given.a_metric_core.with_scan_rate(5).build()
        metrics_net.add_core(core)
        await scanner.start()

        metrics_net.set_value(2, core.name)
        await self._after_passing_a_scan_rate_period(clock, 5)
        changes = await scanner.deliver_changes()
        assert changes[0].timestamp == _TS + 5

        metrics_net.set_value(3, core.name)
        scanner.scan_rate = 100
        await self._after_passing_a_scan_rate_period(clock, 5)
        changes = await scanner.deliver_changes()
        assert changes[0].timestamp == _TS + 10

    async def should_not_scan_metrics_with_scan_rate_zero(
        self, scanner, metrics_net, clock
    ):
        core = given.a_metric_core.with_scan_rate(0).build()
        metrics_net.add_core(core)

        await scanner.start()

        with pytest.raises(TimeoutError):
            metrics_net.set_value(2, core.name)
            await self._after_passing_a_scan_rate_period(clock)
            await scanner.deliver_changes(timeout=0.02)

    async def should_scan_a_metric_with_a_special_scan_rate(
        self, scanner, metrics_net, clock
    ):
        core = given.a_metric_core.with_alias(1).with_scan_rate(100).build()
        metrics_net.add_core(core)
        await scanner.start()

        metrics_net.set_value(2, core.name)
        await self._after_passing_a_scan_rate_period(clock, 100)
        changes = await scanner.deliver_changes(timeout=0.01)
        assert changes[0].timestamp == _TS + 100

    async def should_execute_commands(self, scanner: Scanner, metrics_net, clock):
        core = given.a_metric_core.with_alias(1).build()
        metrics_net.add_core(core)
        await scanner.start()

        await self._after_passing_a_scan_rate_period(clock)
        confirmation = await scanner.execute_command(
            Metric(100, 3, core.data_type, alias=1)
        )
        assert confirmation == Metric(
            _TS + _SCAN_RATE,
            3,
            core.data_type,
            alias=core.alias,
            properties=PropertySet.from_kwargs(
                quality=PropertyValue(192, DataType.Int32)
            ),
        )

        await self._after_passing_a_scan_rate_period(clock)
        with pytest.raises(TimeoutError):
            await scanner.deliver_changes(0.05)

    async def should_report_cancellation_for_writing(self, scanner, metrics_net):
        async def wait_for_long_time(value, metric_name, alias):
            await asyncio.sleep(1)

        metrics_net.write_value = wait_for_long_time
        task = asyncio.create_task(scanner.execute_command(given.a_metric.build()))
        await asyncio.sleep(0)

        task.cancel()  # Cancel a long writing operation
        await task

        confirmation = task.result()
        assert confirmation.properties["quality"].value == Quality.STALE.value

    async def should_report_bad_sigcancellation_for_writing(self, scanner, metrics_net):
        metrics_net.write_value = AsyncMock()
        metrics_net.write_value.side_effect = Exception()

        confirmation = await scanner.execute_command(given.a_metric.build())

        assert confirmation.properties["quality"].value == Quality.BAD.value

    async def should_update_scan_rate_command(self, scanner):
        await scanner.start()

        assert scanner.scan_rate == _SCAN_RATE
        confirmation = await scanner.execute_command(
            Metric(_TS, 300000, DataType.Int64, name="Node Control/Scan Rate")
        )

        assert scanner.scan_rate == 300000
        assert confirmation == Metric(
            _TS,
            300000,
            DataType.Int32,
            name="Node Control/Scan Rate",
        )
