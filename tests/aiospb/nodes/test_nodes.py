import asyncio
from unittest.mock import MagicMock

import pytest

from aiospb.nodes.messages import MqttNodeCarrier
from aiospb.nodes.nodes import EdgeNode
from aiospb.nodes.scanning import Scanner
from aiospb.nodes.states import NodeState
from aiospb.nodes.stores import HistoricalStore

_NODE_NAME = "group/node"


@pytest.fixture
async def node():
    scanner = MagicMock(spec=Scanner)
    scanner.deliver_changes = asyncio.Event().wait
    node = EdgeNode(
        _NODE_NAME,
        MagicMock(spec=MqttNodeCarrier),
        scanner,
        MagicMock(spec=HistoricalStore),
        primary_hostname="primary",
    )

    yield node
    await node.terminate_session()


class An_EdgeNode:
    async def should_starts_disconnected(self, node):
        assert node.state == "disconnected"

    @pytest.mark.asyncio
    async def should_by_pass_methods_to_state(self, monkeypatch):
        StateClass = MagicMock(return_value=MagicMock(spec=NodeState))
        monkeypatch.setattr("aiospb.nodes.nodes.Disconnected", StateClass)
        node = EdgeNode(
            _NODE_NAME,
            MagicMock(spec=MqttNodeCarrier),
            MagicMock(spec=Scanner),
            MagicMock(spec=HistoricalStore),
            primary_hostname="primary",
        )

        await node.establish_session()
        StateClass().establish_session.assert_called_with()

        await node.terminate_session()
        StateClass().terminate_session.assert_called_with()
