import pytest

from aiospb.data import DataType, PropertySet, PropertyValue, Quality
from aiospb.nodes.stores import FsHistoricalStore, Metric, OnmemHistoricalStore


class A_OnmemHistorical:
    @pytest.mark.asyncio
    async def should_save_and_load_changes(self):
        store = OnmemHistoricalStore()
        dto = Metric(
            timestamp=1000,
            value=1,
            data_type=DataType.Int8,
            alias=1,
        )
        await store.save_dtos([dto])

        assert store.has_dtos()
        assert await store.load_all_dtos() == [
            Metric(
                timestamp=1000,
                value=1,
                data_type=DataType.Int8,
                alias=1,
                is_historical=True,
            )
        ]


class A_FsHistoricalStore:
    @pytest.mark.asyncio
    async def should_save_and_load_changes(self, tmp_path):
        filename = f"{tmp_path}/dtos.csv"
        store = FsHistoricalStore(filename)
        assert not store.has_dtos()

        dtos = [
            Metric(
                timestamp=1000,
                value=1,
                data_type=DataType.Int8,
                alias=1,
                name="Metric/Name",
            ),
            Metric(
                timestamp=1000,
                value=1,
                data_type=DataType.Int8,
                properties=PropertySet.from_kwargs(
                    quality=PropertyValue(Quality.BAD.value, DataType.Int32)
                ),
                alias=1,
            ),
        ]
        await store.save_dtos(dtos)
        assert store.has_dtos()

        store = FsHistoricalStore(filename)
        assert store.has_dtos()

        for dto in dtos:
            dto.is_historical = True
        loaded = await store.load_all_dtos()
        assert loaded == dtos
        await store.clear()
        assert not store.has_dtos()

        store = FsHistoricalStore(filename)
        assert not store.has_dtos()
