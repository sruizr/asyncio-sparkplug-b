import asyncio
from unittest.mock import AsyncMock, MagicMock

import pytest

import aiospb.nodes.states as states
from aiospb.data import DataType
from aiospb.mqtt import HostPayload, Metric, MqttError, Topic
from aiospb.nodes.messages import MqttNodeCarrier, NodePayload, SpbMessage
from aiospb.nodes.scanning import Scanner
from aiospb.nodes.states import Connected, Disconnected, NodeContext, Reporting
from aiospb.nodes.stores import HistoricalStore
from tests.builders import Given

given = Given()
_TS = 1000
_HOSTNAME = "primary-host"
_NODENAME = "group/node"


@pytest.fixture
def logger():
    _logger = states.logger
    states.logger = mock = MagicMock()
    yield mock
    states.logger = _logger


@pytest.fixture(autouse=True)
def shell(monkeypatch):
    mock = AsyncMock(return_value=AsyncMock())
    monkeypatch.setattr("aiospb.nodes.states.create_subprocess_shell", mock)
    return mock


@pytest.fixture
def scanner():
    fake_scanner = MagicMock(spec=Scanner)
    fake_scanner.changes = asyncio.Queue()

    async def deliver_changes():
        return await fake_scanner.changes.get()

    fake_scanner.deliver_changes = deliver_changes
    return fake_scanner


@pytest.fixture
def carrier():
    fake_carrier = MagicMock(spec=MqttNodeCarrier)
    fake_carrier.messages = asyncio.Queue()

    async def deliver_message():
        message = await fake_carrier.messages.get()
        return message

    fake_carrier.deliver_message = deliver_message
    return fake_carrier


@pytest.fixture
def store():
    fake_store = MagicMock(spec=HistoricalStore)
    fake_store.has_dtos.return_value = False
    return fake_store


@pytest.fixture
def context(scanner, carrier, store):
    return NodeContext(
        carrier=carrier,
        scanner=scanner,
        store=store,
        primary_hostname=_HOSTNAME,
    )


@pytest.fixture
async def disconnected(context):
    state = Disconnected(context)
    yield state
    await context.state.terminate_session()


@pytest.fixture
async def reporting(context):
    state = Reporting(context)
    yield state
    await context.state.terminate_session()


@pytest.fixture
async def connected(context):
    state = Connected(context)
    yield state
    await context.state.terminate_session()


class A_DisconnectedState:
    async def should_connect_to_mqtt_server(self, disconnected, carrier):
        await disconnected.establish_session()
        carrier.connect.assert_called()

    async def should_try_for_100_seconds_to_connect(
        self, disconnected, carrier, monkeypatch
    ):
        asyncio_module = MagicMock()
        sleep_calls = []

        async def sleep_none(wait):
            sleep_calls.append(wait)
            await asyncio.sleep(0)

        asyncio_module.sleep.side_effect = sleep_none
        monkeypatch.setattr("aiospb.nodes.states.asyncio", asyncio_module)
        carrier.connect.side_effect = MqttError("Not possible to connect")

        task = asyncio.create_task(disconnected.establish_session())
        await asyncio.sleep(0.02)
        task.cancel()

        assert len(sleep_calls) > 1

    async def should_dont_send_birth_when_recieve_node_command(
        self, disconnected, carrier
    ):
        await disconnected.establish_session()

        await carrier.messages.put(
            SpbMessage(
                Topic.from_component("group/node", "NCMD"), NodePayload(1000, [])
            )
        )
        await asyncio.sleep(0)

        carrier.send_birth.assert_not_called()

    async def should_dont_send_birth_when_recieve_no_primary_is_reporting(
        self, disconnected, carrier
    ):
        await disconnected.establish_session()

        await carrier.messages.put(
            SpbMessage(
                Topic.from_component("no-primary", "STATE"),
                HostPayload(_TS, online=True),
            )
        )
        await asyncio.sleep(0)
        carrier.send_birth.assert_not_called()

    async def should_send_birth_when_recieve_primary_is_reporting(
        self, disconnected, carrier, scanner, context
    ):
        await disconnected.establish_session()

        await carrier.messages.put(
            SpbMessage(
                Topic.from_component(_HOSTNAME, "STATE"),
                HostPayload(_TS, online=True),
            )
        )
        await asyncio.sleep(0)

        carrier.send_birth.assert_called_with(scanner.start.return_value)
        scanner.start.assert_called_with()

        await context.state.terminate_session()

    async def should_pass_to_reporting_state_when_host_is_reporting(
        self, disconnected, context, carrier
    ):
        await disconnected.establish_session()

        await carrier.messages.put(
            SpbMessage(
                Topic.from_component(_HOSTNAME, "STATE"),
                HostPayload(_TS, online=True),
            )
        )
        await asyncio.sleep(0)

        assert type(context.state) is Reporting
        await context.state.terminate_session()

    async def should_reconnect_if_error_recieving_messages(self, disconnected, carrier):
        carrier.deliver_message = AsyncMock()
        carrier.deliver_message.side_effect = MqttError()

        await disconnected.establish_session()

        await asyncio.sleep(0)

        assert len(carrier.connect.mock_calls) > 1

    async def should_reconnect_if_error_sending_birth(self, disconnected, carrier):
        carrier.send_birth.side_effect = MqttError()

        await disconnected.establish_session()

        await carrier.messages.put(
            SpbMessage(
                Topic.from_component(_HOSTNAME, "STATE"),
                HostPayload(_TS, online=True),
            )
        )
        await asyncio.sleep(0)

        assert len(carrier.connect.mock_calls) > 1

    async def should_disconnect_if_terminate_session(
        self, disconnected, carrier, scanner
    ):
        await disconnected.establish_session()

        await disconnected.terminate_session()

        carrier.send_death.assert_called_with()
        carrier.disconnect.assert_called_with()
        scanner.stop.assert_called_with()

    @pytest.mark.usefixtures("disconnected")
    async def should_save_scans(self, store, scanner):
        # Simulates error in reporting state to change to disconnected
        changes = [given.a_metric.build()]
        await scanner.changes.put(changes)
        await asyncio.sleep(0)

        store.save_dtos.assert_called_with(changes)

    @pytest.mark.usefixtures("disconnected")
    async def should_logs_warning_if_no_save_scans(self, store, scanner, logger):
        # Simulates error in reporting state to change to disconnected
        changes = [given.a_metric.build()]
        store.save_dtos.side_effect = Exception("Something went bad")
        await scanner.changes.put(changes)
        await asyncio.sleep(0)

        logger.warning.assert_called()


class A_ReportingState:
    @pytest.mark.usefixtures("reporting")
    async def should_send_data_changes(self, carrier, scanner, store):
        changes = [given.a_metric.build()]
        await scanner.changes.put(changes)
        await asyncio.sleep(0)

        carrier.send_data_changes.assert_called_with(changes)

    @pytest.mark.usefixtures("reporting")
    async def should_publish_historical_data(self, scanner, carrier, store):
        history = [given.a_metric.with_alias(1).build()]
        store.has_dtos.return_value = True
        store.load_all_dtos.return_value = history
        changes = [given.a_metric.with_alias(2).build()]
        await scanner.changes.put(changes)

        await asyncio.sleep(0)

        carrier.send_data_changes.assert_called_with(history + changes)
        store.load_all_dtos.assert_called_with()
        store.clear.assert_called_with()

    async def should_publish_certificate_of_death_when_terminate_session(
        self, reporting, context: NodeContext
    ):
        await reporting.terminate_session()
        context.carrier.disconnect.assert_called_with()

    async def _send_node_control_command(self, carrier, name):
        await carrier.messages.put(
            SpbMessage(
                Topic("spBv1.0/group/NCMD/node"),
                NodePayload(
                    timestamp=1100,
                    metrics=[
                        Metric(
                            timestamp=900,
                            value=True,
                            data_type=DataType.Boolean,
                            name=name,
                        )
                    ],
                ),
            )
        )
        await asyncio.sleep(0)

    @pytest.mark.usefixtures("reporting")
    async def should_process_reboot(self, carrier, shell):
        await self._send_node_control_command(carrier, "Node Control/Reboot")

        shell.assert_called_with(
            "reboot", stdout=asyncio.subprocess.PIPE, stderr=asyncio.subprocess.PIPE
        )
        carrier.confirm_commands.assert_called_with(
            [Metric(0, True, DataType.Boolean, name="Node Control/Reboot")]
        )

    @pytest.mark.usefixtures("reporting")
    async def should_process_rebirth(self, carrier, scanner):
        await self._send_node_control_command(carrier, "Node Control/Rebirth")

        carrier.send_birth.assert_called_with(scanner.start.return_value)
        carrier.confirm_commands.assert_called_with(
            [Metric(0, True, DataType.Boolean, name="Node Control/Rebirth")]
        )

    @pytest.mark.usefixtures("reporting")
    async def should_send_commands_to_scanner(self, carrier, scanner):
        commands = [given.a_metric.build()]
        message = SpbMessage(
            Topic.from_component("group/node", "NCMD"), NodePayload(1100, commands)
        )
        await asyncio.wait_for(carrier.messages.put(message), timeout=1)
        await asyncio.sleep(0)

        scanner.execute_command.assert_called_with(commands[0])
        carrier.confirm_commands.assert_called_with(
            [scanner.execute_command.return_value]
        )

    async def should_raise_runtime_if_try_to_establish_session(self, reporting):
        with pytest.raises(RuntimeError):
            await reporting.establish_session()

    @pytest.mark.usefixtures("reporting")
    async def should_log_error_if_any_problem_loading_saved_changes(
        self, scanner, store, logger
    ):
        store.has_dtos.side_effect = Exception()
        await scanner.changes.put([given.a_metric.build()])
        await asyncio.sleep(0)

        logger.error.assert_called()

    @pytest.mark.usefixtures("reporting")
    async def should_change_to_disconnected_if_mqtt_error_when_sending_data(
        self, context
    ):
        context.carrier.send_data_changes.side_effect = MqttError()
        await context.scanner.changes.put([given.a_metric.build()])
        await asyncio.sleep(0.01)

        assert type(context.state) is Disconnected
        context.carrier.connect.assert_called()

        await context.state.terminate_session()

    @pytest.mark.usefixtures("reporting")
    async def should_log_error_if_not_possible_save_changes(self, context, logger):
        context.carrier.send_data_changes.side_effect = MqttError()
        context.store.save_dtos.side_effect = Exception()
        await context.scanner.changes.put([given.a_metric.build()])
        await asyncio.sleep(0)

        logger.error.assert_called()
        await context.state.terminate_session()

    async def should_reconnect_when_error_delivering_host_messages(self, context):
        context.carrier.deliver_message = AsyncMock()
        context.carrier.deliver_message.side_effect = MqttError()

        Reporting(context)
        await asyncio.sleep(0.001)

        context.carrier.connect.assert_called()
        await context.state.terminate_session()

    async def should_reconnect_if_rebirth_fails(self, context):
        context.carrier.send_birth.side_effect = MqttError()
        await self._send_node_control_command(context.carrier, "Node Control/Rebirth")

        Reporting(context)
        await asyncio.sleep(0.001)

        context.carrier.connect.assert_called()
        await context.state.terminate_session()

    @pytest.mark.usefixtures("reporting", "shell")
    async def should_log_warning_when_command_after_reboot(self, carrier, logger):
        await carrier.messages.put(
            SpbMessage(
                Topic("spBv1.0/group/NCMD/node"),
                NodePayload(
                    _TS,
                    [
                        Metric(_TS, True, DataType.Boolean, name="Node Control/Reboot"),
                        Metric(
                            _TS, True, DataType.Boolean, name="Node Control/Rebirth"
                        ),
                    ],
                ),
            )
        )
        await asyncio.sleep(0)

        logger.warning.assert_called()


class A_ConnectedState:
    @pytest.mark.usefixtures("connected")
    async def should_save_scanned_changes(self, scanner, store):
        changes = [given.a_metric.build()]
        await scanner.changes.put(changes)
        await asyncio.sleep(0)

        store.save_dtos.assert_called_with(changes)

    @pytest.mark.usefixtures("connected")
    async def should_change_to_reporting_when_host_is_reporting(self, context, carrier):
        await carrier.messages.put(
            SpbMessage(
                Topic.from_component(_HOSTNAME, "STATE"),
                HostPayload(_TS, online=True),
            )
        )
        await asyncio.sleep(0)

        assert type(context.state) is Reporting
        await context.state.terminate_session()

    @pytest.mark.usefixtures("connected")
    async def should_send_birth_certificate_when_host_is_reporting(
        self, context, carrier
    ):
        await carrier.messages.put(
            SpbMessage(
                Topic.from_component(_HOSTNAME, "STATE"),
                HostPayload(_TS, online=True),
            )
        )
        await asyncio.sleep(0)

        context.carrier.send_birth.assert_called_with(
            context.scanner.start.return_value
        )
        context.scanner.stop.assert_called_with()
        await context.state.terminate_session()

    @pytest.mark.usefixtures("connected")
    async def should_dont_change_to_reporting_if_host_is_not_primary(
        self, context, carrier
    ):
        await carrier.messages.put(
            SpbMessage(
                Topic.from_component("not-primary", "STATE"),
                HostPayload(_TS, online=True),
            )
        )
        await asyncio.sleep(0)

        assert type(context.state) is Connected

    @pytest.mark.usefixtures("connected")
    async def should_dont_change_to_reporting_if_no_state_message(
        self, context, carrier
    ):
        await carrier.messages.put(
            SpbMessage(
                Topic.from_component("group/node", "NCMD"), NodePayload(1000, [])
            )
        )
        await asyncio.sleep(0)

        assert type(context.state) is Connected

    async def should_be_disconnected_if_mqtt_error_waiting_reporting(
        self, context, carrier
    ):
        carrier.deliver_message = AsyncMock(side_effect=MqttError())
        Connected(context)
        await asyncio.sleep(0)

        assert type(context.state) is Disconnected
        await context.state.terminate_session()

    async def should_avoid_establish_session(self, connected):
        with pytest.raises(RuntimeError):
            await connected.establish_session()

    async def should_disconnect_when_terminate_session(self, connected, carrier):
        await connected.terminate_session()

        carrier.disconnect.assert_called_with()

    @pytest.mark.usefixtures("connected")
    async def should_log_warning_if_no_save_changes(self, store, scanner, logger):
        store.save_dtos.side_effect = Exception("Something went bad")
        await scanner.changes.put([given.a_metric.build()])
        await asyncio.sleep(0)

        logger.warning.assert_called()
