from unittest.mock import MagicMock, call

import pytest

from aiospb import Clock
from aiospb.data import DataType
from aiospb.mqtt import Metric, MqttClient, SpbMessage, Topic, Will
from aiospb.nodes.messages import MqttNodeCarrier, NodePayload
from tests.builders import Given

_TS = 1000
_NODE_NAME = "group/node"
given = Given()


@pytest.fixture
def mqtt_client() -> MqttClient:
    mock = MagicMock(spec=MqttClient)
    mock.is_connected = False
    return mock


@pytest.fixture
def clock():
    fake_clock = MagicMock(spec=Clock)
    fake_clock.timestamp.return_value = _TS
    return fake_clock


@pytest.fixture
def carrier(mqtt_client, clock):
    return MqttNodeCarrier(mqtt_client, _NODE_NAME, clock)


class A_MqttNodeCarrier:
    def given_a_will_message(self, bd_seq=0):
        return Will(
            SpbMessage(
                Topic.from_component(_NODE_NAME, "NDEATH"),
                NodePayload(
                    timestamp=_TS,
                    metrics=[Metric(_TS, bd_seq, DataType.Int64, name="bdSeq")],
                ),
            ),
            qos=1,
            retain=False,
        )

    async def should_connect_to_mqtt_server(
        self, carrier: MqttNodeCarrier, mqtt_client
    ):
        will = self.given_a_will_message(bd_seq=0)
        await carrier.connect()

        mqtt_client.connect.assert_called_with("group/node", will=will)
        assert carrier.is_connected == mqtt_client.is_connected

    async def should_increase_circularly_bd_seq_of_will_message(
        self, carrier: MqttNodeCarrier, mqtt_client
    ):
        for bd_seq in range(256):
            await carrier.connect()
            will = self.given_a_will_message(bd_seq=bd_seq)
            mqtt_client.connect.assert_called_with("group/node", will=will)

        await carrier.connect()
        will = self.given_a_will_message(bd_seq=0)
        mqtt_client.connect.assert_called_with("group/node", will=will)

    async def should_subscribe_to_host_messages_when_connect(
        self, carrier, mqtt_client
    ):
        await carrier.connect()
        assert call("spBv1.0/STATE/+", qos=1) in mqtt_client.subscribe.mock_calls

    async def should_subscribe_to_recieve_node_commands_when_connect(
        self, carrier, mqtt_client
    ):
        await carrier.connect()
        assert (
            call("spBv1.0/group/NCMD/node", qos=1) in mqtt_client.subscribe.mock_calls
        )

    async def should_be_connected_before_send_birth_certificate(self, carrier):
        with pytest.raises(RuntimeError):
            await carrier.send_birth([given.a_metric.build()])

    async def should_send_birth_certificate(
        self, carrier: MqttNodeCarrier, mqtt_client
    ):
        await carrier.connect()
        metrics = [given.a_metric.build()]

        await carrier.send_birth(metrics)

        mqtt_client.publish.assert_called_with(
            SpbMessage(
                Topic("spBv1.0/group/NBIRTH/node"),
                NodePayload(
                    _TS, metrics + [Metric(_TS, 0, DataType.Int64, name="bdSeq")], 0
                ),
            ),
            qos=0,
            retain=False,
        )

    async def should_be_connected_before_send_any_data_change(self, carrier):
        with pytest.raises(RuntimeError):
            await carrier.send_data_changes([given.a_metric.build()])

    async def should_send_data_changes(self, carrier: MqttNodeCarrier, mqtt_client):
        await carrier.connect()
        metrics = [given.a_metric.build()]
        await carrier.send_birth(metrics)

        await carrier.send_data_changes(metrics)

        mqtt_client.publish.assert_called_with(
            SpbMessage(
                Topic("spBv1.0/group/NDATA/node"),
                NodePayload(_TS, metrics, 1),
            ),
            qos=0,
            retain=False,
        )

    async def should_increase_sequence_circularly_when_data_changes(
        self, carrier: MqttNodeCarrier, mqtt_client
    ):
        await carrier.connect()
        metrics = [given.a_metric.build()]
        await carrier.send_birth(metrics)

        for index in range(255):
            await carrier.send_data_changes(metrics)
            mqtt_client.publish.assert_called_with(
                SpbMessage(
                    Topic("spBv1.0/group/NDATA/node"),
                    NodePayload(_TS, metrics, index + 1),
                ),
                qos=0,
                retain=False,
            )

        await carrier.send_data_changes(metrics)
        mqtt_client.publish.assert_called_with(
            SpbMessage(
                Topic("spBv1.0/group/NDATA/node"),
                NodePayload(_TS, metrics, 0),
            ),
            qos=0,
            retain=False,
        )

    async def should_be_connected_before_send_death_certificate(self, carrier):
        with pytest.raises(RuntimeError):
            await carrier.send_death()

    async def should_send_death_certificate(
        self, carrier: MqttNodeCarrier, mqtt_client
    ):
        await carrier.connect()

        await carrier.send_death()
        mqtt_client.publish.assert_called_with(
            SpbMessage(
                Topic.from_component(_NODE_NAME, "NDEATH"),
                NodePayload(_TS, [Metric(_TS, 0, DataType.Int64, name="bdSeq")]),
            ),
            qos=1,
            retain=False,
        )

    async def should_be_disconnected(self, carrier: MqttNodeCarrier, mqtt_client):
        await carrier.disconnect()

        mqtt_client.disconnect.assert_called_with()

    async def should_assure_is_connected_before_confirm_commands(self, carrier):
        with pytest.raises(RuntimeError):
            await carrier.confirm_commands([given.a_metric.build()])

    async def should_confirm_commands(self, carrier: MqttNodeCarrier, mqtt_client):
        await carrier.connect()
        writings = [
            given.a_metric_core.with_alias(1).build().create_metric(10, 500),
            given.a_metric_core.with_alias(2).build().create_metric(10, 0),
        ]

        await carrier.confirm_commands(writings)

        writings[1].timestamp = _TS
        mqtt_client.publish.assert_called_with(
            SpbMessage(
                Topic.from_component(_NODE_NAME, "NDATA"),
                NodePayload(
                    _TS,
                    writings,
                    seq=0,
                ),
            ),
            qos=0,
            retain=False,
        )
