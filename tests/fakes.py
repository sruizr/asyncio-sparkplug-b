import asyncio
from datetime import datetime

from aiospb import Clock
from aiospb.data import Metric, Quality, ValueType
from aiospb.mqtt import MqttClient, SpbMessage, Will
from aiospb.nodes import EdgeNode, MetricsNet
from aiospb.nodes.scanning import MetricCore, MetricNotFound, Scanner


class FakeClock(Clock):
    def __init__(self, ts_0=1000):
        self._ts = ts_0
        self._event = None

    def now(self) -> datetime:
        return datetime.fromtimestamp(self._ts / 1000)

    def timestamp(self):
        return self._ts

    def sleep(self, seconds=1.0):
        self._ts += int(seconds * 1000)
        print(f"incremented {seconds} s")
        if self._event and self._ts >= self._sleep_till:
            self._event.set()

    async def asleep(self, seconds=1.0):
        self._sleep_till = self._ts + int(seconds * 1000)
        self._event = asyncio.Event()
        await asyncio.sleep(0)
        await self._event.wait()
        self._event = None


class FakeMqttClient(MqttClient):
    def __init__(self):
        super().__init__()
        self._messages = asyncio.Queue()
        self.subscriptions = []
        self.last_will_testament = None
        self.publications = []
        self._is_connected = False

    @property
    def is_connected(self):
        return self._is_connected

    async def subscribe(self, topic: str, qos=0):
        self.subscriptions.append((topic, qos))

    async def deliver_message(self):
        return await self._messages.get()

    def add_publication(self, message: SpbMessage):
        self._messages.put_nowait(message)

    async def connect(self, component_name: EdgeNode, will: Will):
        self.component_name = component_name
        self.last_will_testament = will
        self._is_connected = True

    async def publish(self, message: SpbMessage, qos: int = 0, retain: bool = False):
        self.publications.append((message, qos, retain))
        await asyncio.sleep(0)

    async def disconnect(self):
        self._is_connected = False


class FakeMetricsNet(MetricsNet):
    def __init__(self):
        self._cores = []
        self._values = {}
        self.writting_quality = Quality.GOOD
        self.writtings = []

    def add_core(self, core: MetricCore):
        self._cores.append(core)

    def set_value(
        self, value: ValueType | Exception, metric_name: str = "", alias: int = 0
    ):
        core = self._find_core(metric_name, alias)
        self._values[core] = value

    async def get_metric_cores(self, _: str = "") -> list[MetricCore]:
        return self._cores

    def _find_core(self, metric_name, alias):
        for core in self._cores:
            if metric_name and core.name == metric_name:
                return core
            if alias and alias == core.alias:
                return core
        raise MetricNotFound(f"Not found core {metric_name}:{alias}")

    async def read_value(self, metric_name: str = "", alias: int = 0) -> ValueType:
        core = self._find_core(metric_name, alias)

        if core not in self._values:
            return

        value = self._values[core]
        if type(value) is Exception:  # Bad quality simulation if value is exception
            raise value

        if value is None:  # Stale option if value is None
            await asyncio.sleep(1000)

        return value

    async def write_value(
        self, value: ValueType | Exception, metric_name: str = "", alias: int = 0
    ):
        if type(value) is Exception:
            raise value

        core = self._find_core(metric_name, alias)
        self.writtings.append((value, metric_name, alias))

        self._values[core] = value


class FakeScanner(Scanner):
    def __init__(self, scan_rate=60.0):
        self._running = False
        self._scan_rate = scan_rate
        self.births = []
        self._queue = asyncio.Queue()
        self._net = None

    def add_birth(self, dto: Metric):
        self.births.append(dto)

    def add_changes(self, changes: list[Metric]):
        self._queue.put_nowait(changes)

    @property
    def scan_rate(self):
        return self._scan_rate

    @scan_rate.setter
    def scan_rate(self, value):
        self._scan_rate = value

    @property
    def is_running(self):
        return self._running

    async def start(self):
        self._running = True
        return self.births

    async def deliver_changes(self) -> list[SpbMessage]:
        self.changes = await self._queue.get()
        return self.changes

    async def stop(self):
        self._running = False
